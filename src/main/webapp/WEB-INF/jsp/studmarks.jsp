<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Log in with your account</title>
  <link rel="stylesheet" type="text/css" href="${contextPath}/resources/css/style.css">


  <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
  <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
</head>

<body>
<div class="container">
  <table>
    <thead>
    <th>Курс</th>
    <th>Оценка</th>
    </thead>
    <c:forEach items = "${allMarks}" var = "row">
      <tr>
        <td>
          <c:out value="${row.key}"/></td> <td><c:out value="${row.value}"/>
        </td>
      </tr>
    </c:forEach>
  </table>
  <h4></h4>
  <a href="/">Главная</a>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>
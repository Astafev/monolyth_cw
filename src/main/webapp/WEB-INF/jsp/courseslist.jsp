<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Список курсов</title>


    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <td>
        <h4>Студент: <c:out value="${student.name}"/> <c:out value="${student.surname}"/></h4>
    </td>
    <h1></h1>
    <h4>Список курсов:</h4>
    <h1></h1>
    <table>
        <c:forEach items="${coursesList}" var="item">
            ${item}<br>
        </c:forEach>
    </table>
    <h1></h1>
    <a href="/">Главная</a>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>
package com.boots.controller;

import com.boots.entity.Student;
import com.boots.service.StudentService;
import com.boots.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

@Controller
public class StudentController {


    @Autowired
    private UserService userService;

    @Autowired
    private StudentService studService;

    @GetMapping("/studmarks")
    public String studentMarks(Model model){

        List<Student> studs = userService.findStudentUser(userService.getCurrentUsername());
        Long stud_id = studs.get(0).getId();
        HashMap<String,Integer> marks = null;
        try {
            marks = studService.getStudMarks(stud_id);
        } catch (Exception ex) {
        }

        model.addAttribute("allMarks", marks);
        return "studmarks";
    }

    @GetMapping("/courseslist")
    public String coursesList(Model model) throws SQLException, ClassNotFoundException {

        List<Student> studs = userService.findStudentUser(userService.getCurrentUsername());
        Long stud_id = studs.get(0).getId();
        List<String> courses = studService.getStudentCourses(stud_id);

        model.addAttribute("student",studs.get(0));
        model.addAttribute("coursesList",courses);
        return "courseslist";
    }

}

package com.boots.service;

import com.boots.entity.Course;
import com.boots.entity.Mark;
import com.boots.entity.Student;
import com.boots.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class StudentService {

    @Autowired
    private StudentRepository studRepo;

    @PersistenceContext
    private EntityManager em;

    public HashMap<String,Integer> getStudMarks(Long stud_id) throws ClassNotFoundException, SQLException {

        String JDBC_DRIVER = "com.mysql.jdbc.Driver";
        String DB_URL = "jdbc:postgresql://localhost:5432/research_db";

        Class.forName("org.postgresql.Driver");
        Connection connection = DriverManager.getConnection(DB_URL, "postgres", "postgres");

        Statement statement = connection.createStatement();

        ResultSet rs = statement.executeQuery("select c.name, m.mark from t_marks m\n" +
                "inner join t_course c on c.id = m.course_id\n" +
                "inner join t_student s on s.id = m.stud_id\n" +
                "where s.id = " + stud_id + ";");

        HashMap<String,Integer> table = new HashMap<>();

        while(rs.next()){

            String course_name = rs.getString("name");
            int mark = rs.getInt("mark");

            table.put(course_name, mark);
        }
        rs.close();

        return table;
    }

    public List<String> getStudentCourses(Long stud_id) throws ClassNotFoundException, SQLException {

        String JDBC_DRIVER = "com.mysql.jdbc.Driver";
        String DB_URL = "jdbc:postgresql://localhost:5432/research_db";

        Class.forName("org.postgresql.Driver");
        Connection connection = DriverManager.getConnection(DB_URL, "postgres", "postgres");

        Statement statement = connection.createStatement();

        ResultSet rs = statement.executeQuery("select c.name from t_course c\n" +
                "inner join t_marks tm on c.id = tm.course_id\n" +
                "inner join t_student ts on tm.stud_id = ts.id\n" +
                "where ts.id = " + stud_id + ";");

        List<String> courses = new ArrayList<>();

        while(rs.next())
            courses.add(rs.getString("name"));
        rs.close();

        return courses;
    }

}

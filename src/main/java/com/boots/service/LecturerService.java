package com.boots.service;

import com.boots.entity.Course;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.*;
import java.util.HashMap;
import java.util.List;

@Service
public class LecturerService {

    @PersistenceContext
    private EntityManager em;

    public List<Course> findLecturerCourse(Long lecturer_id){

        return em.createNativeQuery("select * from t_course c \n" +
                "inner join t_lecturer tl on c.id = tl.course_id\n" +
                "where tl.id = :lecturer_id", Course.class)
                .setParameter("lecturer_id", lecturer_id).getResultList();
    }

    public HashMap<String,Integer> getCourseMarks(String courseName) throws ClassNotFoundException, SQLException {

        String JDBC_DRIVER = "com.mysql.jdbc.Driver";
        String DB_URL = "jdbc:postgresql://localhost:5432/research_db";

        Class.forName("org.postgresql.Driver");
        Connection connection = DriverManager.getConnection(DB_URL, "postgres", "postgres");

        Statement statement = connection.createStatement();

        ResultSet rs = statement.executeQuery("select s.name, s.surname, m.mark from t_student s\n" +
                "inner join t_marks m on s.id = m.stud_id \n" +
                "inner join t_course c on m.course_id = c.id \n" +
                "where c.name = \'" + courseName + "\';");

        HashMap<String,Integer> stud_marks = new HashMap<>();

        while(rs.next()){

            String student = rs.getString("name") + " " + rs.getString("surname");
            int mark = rs.getInt("mark");

            stud_marks.put(student, mark);
        }
        rs.close();

        return stud_marks;
    }
}

package com.boots.entity.persisted;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class TUserRolesEntityPK implements Serializable {
    private long userId;
    private long rolesId;

    @Column(name = "user_id")
    @Id
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Column(name = "roles_id")
    @Id
    public long getRolesId() {
        return rolesId;
    }

    public void setRolesId(long rolesId) {
        this.rolesId = rolesId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TUserRolesEntityPK that = (TUserRolesEntityPK) o;
        return userId == that.userId &&
                rolesId == that.rolesId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, rolesId);
    }
}

package com.boots.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "t_course")
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

//    @OneToMany(fetch = FetchType.LAZY,
//               cascade = CascadeType.ALL)
//    @JoinTable(name = "t_marks",
//               joinColumns = @JoinColumn(name = "course_id"))
//    private List<Mark> course_marks;

    public Course() {
    }

    public Course(String name, String description){
        this.name = name;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

//    public List<Mark> getCourse_marks() {
//        return course_marks;
//    }
//
//    public void setCourse_marks(List<Mark> course_marks) {
//        this.course_marks = course_marks;
//    }
}

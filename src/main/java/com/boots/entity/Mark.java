package com.boots.entity;

import javax.persistence.*;

@Entity
@Table(name = "t_marks")
public class Mark {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "mark")
    private Long mark;

    @Column(name = "stud_id")
    private Long stud_id;

    @Column(name = "course_id")
    private Long course_id;

//    @ManyToOne(fetch = FetchType.EAGER)
//    @JoinTable(name = "t_student",
//               joinColumns = @JoinColumn(name = "id"))
//    private Student student;

//    @ManyToOne(fetch = FetchType.EAGER)
//    @JoinTable(name = "t_course",
//               joinColumns = @JoinColumn(name = "id"))
//    private Course course;

    public Mark() {
    }

    public Mark(Long mark, Long stud_id, Long course_id){
        this.mark = mark;
        this.stud_id = stud_id;
        this.course_id = course_id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMark() {
        return mark;
    }

    public void setMark(Long mark) {
        this.mark = mark;
    }

    public Long getStud_id() {
        return stud_id;
    }

    public void setStud_id(Long stud_id) {
        this.stud_id = stud_id;
    }

    public Long getCourse_id() {
        return course_id;
    }

    public void setCourse_id(Long course_id) {
        this.course_id = course_id;
    }

//    public Student getStudent() {
//        return student;
//    }
//
//    public void setStudent(Student student) {
//        this.student = student;
//    }
//
//    public Course getCourse() {
//        return course;
//    }
//
//    public void setCourse(Course course) {
//        this.course = course;
//    }
}
